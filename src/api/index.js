import Axios from "axios";

const baseURL = "http://128.199.231.142";
const userId = "EEA466F0-1A72-4B7D-8393-04B3D6CF0695";
const lat = "37.3926383";
const lag = "122.1279267";
const dist = "200";
let page = "0";

// const instance = Axios.create({ baseURL });

// instance.defaults.proxy = "http://128.199.231.142";
const getAllPosts = async (onSucc, onErr) => {
  Axios.get(`https://cors-anywhere.herokuapp.com/${baseURL}/post/65`, {
    params: {
      userId,
    },
  })
    .then((res) => {
      onSucc(res.data);
    })
    .catch((err) => onErr(err));
};

const savePost = async (title, body) => {
  try {
    const res = await Axios.post(baseURL, {
      title,
      body,
      userId,
      postTypeId: 4,
      lat,
      lag,
      page,
    });
    console.log(res);
    if (res.status === "200") return "Seccessfully Saved!";
  } catch (error) {
    console.log(error);
  }
};

export { getAllPosts, savePost };
