import React, { useState, useEffect } from "react";
import { getAllPosts, savePost } from "../api";

export default function Post() {
  const [post, setPost] = useState({});

  const onSucc = (data) => {
    setPost(data);
  };

  const onError = (err) => {
    console.log(err);
  };

  useEffect(() => {
    getAllPosts(onSucc, onError);
    console.log("p", post);
  }, []);

  return (
    <>
      <div>
        <title>Title</title>
        <p>{post.title}</p>
      </div>
      <div>
        <h3>Post</h3>
        <p>{post.body}</p>
      </div>
    </>
  );
}
